#include "TypeDefine.h"
#include "Mavlink_types.h"
#include "Checksum.h"
#include "USART.h"

#define MAVLINK_STX 254

mavlink_system_t   mavlink_system = {1,1};

void _mav_finalize_message_chan_send(mavlink_channel_t chan, uint8_t msgid,  char *packet, uint8_t length, uint8_t crc_extra)
{
	uint16_t checksum;
	uint8_t buf[MAVLINK_NUM_HEADER_BYTES];
	uint8_t ck[2];
	//mavlink_status_t *status = mavlink_get_channel_status(chan);
	buf[0] = MAVLINK_STX;
	buf[1] = length;
	//buf[2] = status->current_tx_seq;
	buf[2] = __mavlink_status_current_tx_seq;   //PS:自定义
	buf[3] = mavlink_system.sysid;
	buf[4] = mavlink_system.compid;
	buf[5] = msgid;
	//status->current_tx_seq++;
	checksum = crc_calculate((uint8_t*)&buf[1], MAVLINK_CORE_HEADER_LEN);
	crc_accumulate_buffer(&checksum, packet, length);
	crc_accumulate(crc_extra, &checksum);

	ck[0] = (uint8_t)(checksum & 0xFF);
	ck[1] = (uint8_t)(checksum >> 8);

	//MAVLINK_START_UART_SEND(chan, MAVLINK_NUM_NON_PAYLOAD_BYTES + (uint16_t)length);
	//_mavlink_send_uart(chan, (const char *)buf, MAVLINK_NUM_HEADER_BYTES);
	//_mavlink_send_uart(chan, packet, length);
    //_mavlink_send_uart(chan, (const char *)ck, 2);
	//MAVLINK_END_UART_SEND(chan, MAVLINK_NUM_NON_PAYLOAD_BYTES + (uint16_t)length);

	//自定义串口发送程序
	Uart1_PutString((char *)buf , MAVLINK_NUM_HEADER_BYTES);
	Uart1_PutString((char *)packet , length);
	Uart1_PutString((char *)ck , 2);

}
