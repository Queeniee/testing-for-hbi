#define MAVLINK_MSG_ID_AHRS_LEN 28
#define MAVLINK_MSG_ID_AHRS 163
#define MAVLINK_MSG_ID_AHRS_CRC 127


typedef struct __mavlink_ahrs_t
{
 float omegaIx; ///< X gyro drift estimate rad/s
 float omegaIy; ///< Y gyro drift estimate rad/s
 float omegaIz; ///< Z gyro drift estimate rad/s
 float accel_weight; ///< average accel_weight
 float renorm_val; ///< average renormalisation value
 float error_rp; ///< average error_roll_pitch value
 float error_yaw; ///< average error_yaw value
} mavlink_ahrs_t;


void mavlink_msg_ahrs_send(mavlink_channel_t chan, float omegaIx, float omegaIy, float omegaIz, float accel_weight, float renorm_val, float error_rp, float error_yaw)
{
	mavlink_ahrs_t packet;
	packet.omegaIx = omegaIx;
	packet.omegaIy = omegaIy;
	packet.omegaIz = omegaIz;
	packet.accel_weight = accel_weight;
	packet.renorm_val = renorm_val;
	packet.error_rp = error_rp;
	packet.error_yaw = error_yaw;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_AHRS, (char *)&packet, MAVLINK_MSG_ID_AHRS_LEN, MAVLINK_MSG_ID_AHRS_CRC);

}
