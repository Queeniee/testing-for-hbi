#include "stm32f10x.h"
#include "bsp_usart1.h"
#define uint16_t unsigned short
#define uint8_t unsigned char
#define X25_INIT_CRC 0xffff
#define MAVLINK_CORE_HEADER_LEN 5

#define MAVLINK_MSG_ID_HEARTBEAT_CRC 50
//	uint8_t _buf[6];
	uint8_t ck[2];
 


void crc_init(uint16_t* crcAccum)
{
        *crcAccum = X25_INIT_CRC;
}

void crc_accumulate(uint8_t data, uint16_t *crcAccum)
{
        /*Accumulate one byte of data into the CRC*/
        uint8_t tmp;

        tmp = data ^ (uint8_t)(*crcAccum &0xff);
        tmp ^= (tmp<<4);
        *crcAccum = (*crcAccum>>8) ^ (tmp<<8) ^ (tmp <<3) ^ (tmp>>4);
}

uint16_t crc_calculate(const uint8_t* pBuffer, uint16_t length)
{
        uint16_t crcTmp;
        crc_init(&crcTmp);
	while (length--) {
                crc_accumulate(*pBuffer++, &crcTmp);
        }
        return crcTmp;
}

void crc_accumulate_buffer(uint16_t *crcAccum,  uint8_t *pBuffer, uint8_t length)
{
	const uint8_t *p = (const uint8_t *)pBuffer;
	while (length--) {
                crc_accumulate(*p++, crcAccum);
        }
}

	  

uint8_t *UARTCRC(uint8_t data[15],uint8_t crc_extra)
{

	uint16_t checksum;

	uint8_t buf[9]={0x05,0x00,0x00,0x00,0x0d,0x03,0x59,0x03,0x03};
	
	checksum = crc_calculate((uint8_t*)&data[1], 5);

	crc_accumulate_buffer(&checksum, buf, 9);

	crc_accumulate(crc_extra, &checksum);

	ck[0] = (uint8_t)(checksum & 0xFF);
	ck[1] = (uint8_t)(checksum >> 8);
	
	return ck;
	
}
