#include "stm32f10x.h"
#include <stdio.h>
#define uint16_t unsigned short
#define uint8_t unsigned char
#define MAVLINK_MSG_ID_ATTITUDE_LEN 28
#define MAVLINK_MSG_ID_30_LEN 28

#define MAVLINK_MSG_ID_ATTITUDE 30




typedef struct __mavlink_attitude_t
{
 uint32_t time_boot_ms; ///< Timestamp (milliseconds since system boot)
 float roll; ///< Roll angle (rad)
 float pitch; ///< Pitch angle (rad)
 float yaw; ///< Yaw angle (rad)
 float rollspeed; ///< Roll angular speed (rad/s)
 float pitchspeed; ///< Pitch angular speed (rad/s)
 float yawspeed; ///< Yaw angular speed (rad/s)
} mavlink_attitude_t;


typedef union   
{  
    struct roll   
    {  
        unsigned char low_roll;  
        unsigned char mlow_roll;  
        unsigned char mhigh_roll;  
        unsigned char high_roll;  
     }float_roll;  

	 struct pitch   
    {  
        unsigned char low_pitch;  
        unsigned char mlow_pitch;  
        unsigned char mhigh_pitch;  
        unsigned char high_pitch;  
     }float_pitch; 

	 struct yaw   
    {  
        unsigned char low_yaw;  
        unsigned char mlow_yaw;  
        unsigned char mhigh_yaw;  
        unsigned char high_yaw;  
     }float_yaw; 

	 struct rollspeed   
    {  
        unsigned char low_rollspeed;  
        unsigned char mlow_rollspeed;  
        unsigned char mhigh_rollspeed;  
        unsigned char high_rollspeed;  
     }float_rollspeed; 

	 struct pitchspeed   
    {  
        unsigned char low_pitchspeed;  
        unsigned char mlow_pitchspeed;  
        unsigned char mhigh_pitchspeed;  
        unsigned char high_pitchspeed;  
     }float_pitchspeed; 

	 struct yawspeed   
    {  
        unsigned char low_yawspeed;  
        unsigned char mlow_yawspeed;  
        unsigned char mhigh_yawspeed;  
        unsigned char high_yawspeed;  
     }float_yawspeed; 
            
     float  value;  
}FLAOT_UNION; 



 

